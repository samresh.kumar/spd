$(document).ready(function () {

  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 20) {
        $(".header").addClass("darkHeader");
    }
    else{
      $(".header").removeClass("darkHeader");
    }
}); 



  $('.home-banner .owl-carousel').owlCarousel({
    loop:true,
    responsiveClass:true,
    //autoplay:2000,
    animateOut: 'fadeOut',
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:true
        }
    }
  })
  $('.home-what-wedo-slider .owl-carousel').owlCarousel({
    loop:false,
    responsiveClass:true,
    margin:20,
    //autoplay:2500,
    //smartSpeed: 4000,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
    }
  })

  $('.home-succ-right .owl-carousel').owlCarousel({
    loop:true,
    responsiveClass:true,
    // autoplay:3000,
    // smartSpeed: 4000,
    margin:20,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            loop:true
        }
    }
  })

  $('.home-testimonial-right .owl-carousel').owlCarousel({
    loop:true,
    responsiveClass:true,
    //autoplay:3000,
    margin:20,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            loop:false
        }
    }
  })

  $('.home-mdeia-slider .owl-carousel').owlCarousel({
    loop:false,
    responsiveClass:true,
    //autoplay:3000,
   // smartSpeed: 4000,
    margin:20,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
  })

  $('[data-toggle="tooltip"]').tooltip()

//     var pixelToMove = 50;
//     $(".boy-pic").mousemove(function(e) {
//       var width = $(this).innerWidth();
//       var height = $(this).innerHeight();
//       var newValueX = (e.pageX / width) * pixelToMove;
//       var newValueY = (e.pageY / height) * pixelToMove;
//       $(this).css('background-position', newValueX + '%' + ' ' + newValueY + '%');
//     });
$.fn.parallax = function ( resistance, mouse ) 
{
	$el = $( this );
	TweenLite.to( $el, 0.2, 
	{
		x : -(( mouse.clientX - (window.innerWidth/2) ) / resistance ),
		y : -(( mouse.clientY - (window.innerHeight/2) ) / resistance )
	});

};
$(".home-banner").mousemove( function( e ) {
    $( '.banner-boy' ).parallax( 20 , e );
    $( '.banner-icon1' ).parallax( 50 , e );
    $( '.banner-icon2' ).parallax( 40 , e );
    $( '.banner-icon3' ).parallax( 30 , e );
    $( '.banner-icon4' ).parallax( 45 , e );
    $( '.banner-icon5' ).parallax( 25 , e );
    $( '.banner-icon6' ).parallax( 20 , e );
});

$('.mobile-menu a').on('click', () => {
    $('.mobile-menu a .fa').toggleClass('fa-times');
    $('.mobile-menu a .fa').toggleClass('fa-bars');
    $('.navigation').toggleClass('open');
    $('.black-over').toggle();
});

$('.black-over').on('click', () =>{
    $('.navigation').removeClass('open');  
    $('.black-over').hide();
    $('.mobile-menu a .fa').removeClass('fa-times');
    $('.mobile-menu a .fa').addClass('fa-bars');
})

$('.close').click(function(){
    $('.yvideo').each(function(){
    $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
  });
});



});